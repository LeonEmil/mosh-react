import React from 'react';
import Navbar from "./Navbar"
import CounterList from "./CounterList"

class App extends React.Component {
    state = { 
        counters: [
            {id: 0, value: 1},
            {id: 1, value: 0},
            {id: 2, value: 0}
        ]
     }

     handleIncrement = (id) => {
        const newCounters = this.state.counters.map(counter => {
            if(counter.id === id){
                counter.value += 1
                return counter
            }
            else {
                return counter
            }
        })

        this.setState({
            counters: newCounters
        })
     }

     handleReset = () => {
         const newCounters = this.state.counters.map(counter => {
            counter.value = 0 
            return counter
        })
         this.setState({
             counters: newCounters
         })
     }

     handleDelete = (id) => {
         let newCounters = this.state.counters.filter(counter => counter.id !== id)
         let index = 0
         newCounters = newCounters.map(counter => {
             counter.id = index
             index++
             return counter
        })
         this.setState({
             counters: newCounters
         })
     }

     
     render() { 
         const total = this.state.counters.reduce((result, counter) => {
            return result + counter.value
         }, 0)
        return ( 
            <>
                <Navbar total={total}/>
                <main className="container">
                    <button className="btn btn-primary m-2"onClick={this.handleReset}>Reset</button>
                    <CounterList 
                        counters={this.state.counters} 
                        increment={this.handleIncrement}
                        delete={this.handleDelete}
                    />
                </main>
            </>
         );
    }
}
 
export default App;
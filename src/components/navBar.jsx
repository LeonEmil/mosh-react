import React from 'react';

const NavBar = ({total}) => {
    return ( 
        <nav className="m-5">
            <span>Elements: {total}</span>
        </nav>
    );
}
 
export default NavBar;
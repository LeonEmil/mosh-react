import React from "react"
import Counter from "./Counter"

const CounterList = (props) => {
    return (  
        <>
            {props.counters.map(counter => 
                <Counter 
                    key={counter.id} 
                    id={counter.id} 
                    counters={props.counters}
                    increment={props.increment}
                    delete={props.delete}
                />
            )}
        </>
    );
}
 
export default CounterList;
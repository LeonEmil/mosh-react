import React from 'react';

const Counter = (props) => {
    let index = props.id
    console.log(props)
    return ( 
        <div>
            <span className="">{props.counters[index].value}</span>
            <button className="btn btn-secondary btn-sm m-2" onClick={() => props.increment(props.id)}>Increment</button>
            <button className="btn btn-danger btn-sm m-2" onClick={() => props.delete(props.id)}>Delete</button>
        </div>
     );
}
 
export default Counter;

// getBadgeClasses() {
//     let classes = "badge badge-"
//     classes += this.state.count === 0 ? "warning" : "secondary"
//     return classes
// }

// formatCount() {
//     const { count } = this.state
//     return count === 0 ? "Zero" : count
// }
